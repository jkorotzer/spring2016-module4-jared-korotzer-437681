import sys
import re
import collections
import math

if len(sys.argv) == 1:
    print "Please enter a file name."
else:
    filename = sys.argv[1]

text_file = open(filename, "r")
averages = {}
final_averages = {}

#loop through the lines in the file
for line in text_file:
    regex = "(?P<name>[\w\s]+)\sbatted\s(?P<times>\d)[\w\s]+with\s(?P<hit>\d)"
    result = re.match(regex, line)
    if result:
        name = result.group("name")
        times = (int)(result.group("times"))
        hits = (int)(result.group("hit"))
        if name not in averages:
            entry = [times, hits]
            averages[name] = entry
        else:
            averages[name][0] += times
            averages[name][1] += hits
    

for name in averages:
    bat = round(float(averages[name][1]) / (averages[name][0]), 3)
    final_averages[name] = bat
    
#http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value
for thing in sorted(final_averages, key=final_averages.get, reverse=True):
    print "%s : %.3f" % (thing, final_averages[thing])
    